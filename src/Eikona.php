<?php

namespace lucatomasi\Eikona;

use lucatomasi\XFunc\XFunc;

/**
 * @author Luca Tomasi | lucatomasi77@gmail.com
 * @version 1.0.1
*/

/**
 * Class Eikona
 * @package lucatomasi\Eikona
 */
class Eikona {

    private static $galleries = [];

    public static function createGallery(): Gallery {
        $gallery = new Gallery(self::generateID());
        array_push(self::$galleries, $gallery);
        return $gallery;
    }

    public static function getJSFiles($addToXFunc=false): array {
        $url = XFunc::getPathOf(__FILE__)['url'] . 'utils/js/';
        $jsFiles = [
            $url . 'Gallery.js',
            $url . 'Eikona.js'
        ];
        if (!$addToXFunc) return $jsFiles;
        XFunc::addJSFiles($jsFiles);
        return [];
    }

    public static function getCSSFiles($addToXFunc=false): array {
        $url = XFunc::getPathOf(__FILE__)['url'] . 'utils/css/';
        $cssFiles = [
            $url . 'gallery.css'
        ];
        if (!$addToXFunc) return $cssFiles;
        XFunc::addCSSFiles($cssFiles);
        return [];
    }

    private static function generateID(): string {
        $id = null;
        do {
            $id = XFunc::getRandomString(7, false);
            foreach (self::$galleries as $key => $g) {
                if ($key === $id) $id = null;
            }
        } while(!$id);
        return $id;
    }

}