<?php

namespace lucatomasi\Eikona;

class Gallery {

    private $id;

    private $images = [];

    public function __construct(string $id) {
        $this->id = $id;
    }

    public function add(string $url, string $text=null) {
        array_push($this->images, [
            'url' => $url,
            'text' => $text
        ]);
    }

    public function render(): string {
        $out = "<div class='eikona-gallery-container' id='eikona-gallery-{$this->id}'>";

        foreach ($this->images as $image) {
            $text = '';
            if ($image['text']) $text = "<div class='eikona-gallery-text'>{$image['text']}</div>";
            $out .= "<div class='eikona-gallery-image' style='background-image: url(\"{$image['url']}\")'>{$text}</div>";
        }

        return $out . '</div>';
    }

}