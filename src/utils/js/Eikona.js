class Eikona {

    constructor(containersClass) {
        this.galleries = {};
        this.containers = $('.' + containersClass);
        this.containers.each((index) => {
            let galleryID = $(this.containers[index]).attr('id');
            this.galleries[galleryID] = new Gallery(galleryID);
        });
    }

    static openFullScreen() {
        let elem = document.body;
        // Supports most browsers and their versions.
        let requestMethod = elem.requestFullScreen || elem.webkitRequestFullScreen || elem.mozRequestFullScreen || elem.msRequestFullScreen;
        if (requestMethod) { // Native full screen.
            requestMethod.call(elem);
        }
    }

    static closeFullScreen() {
        // Supports most browsers and their versions.
        let requestMethod = document.exitFullscreen || document.webkitExitFullscreen || document.mozCancelFullScreen || document.msExitFullscreen;
        if (requestMethod) { // Exit native full screen.
            requestMethod.call(document);
        }
    }

}

let eikonaGallery = null;

$(document).ready(() => {
    eikonaGallery = new Eikona('eikona-gallery-container');
});