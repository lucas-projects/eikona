class Gallery {
    constructor(galleryID) {

        this.currentElem = false;

        this.hideBtnsTimer = null;

        this.container = $('#' + galleryID);
        this.container.click((event) => {
            this.isMouseOver(event, (elem) => {
                this.currentElem = elem;
                this.open(elem);
            });
        });

        $('body').append('<div class="eikona-popup-outer" id="eikona-popup-outer-'  + galleryID + '"></div>');
        this.popup = $('#eikona-popup-outer-' + galleryID)
            .hide()
            .append(
                '<div class="eikona-popup-inner">' +
                '    <div class="eikona-popup-images" id="eikona-popup-images-'  + galleryID + '"></div>' +
                '    <div class="eikona-popup-buttons">' +
                '        <div class="eikona-popup-btn-previous" id="eikona-popup-btn-previous-'  + galleryID + '"><</div>' +
                '        <div class="eikona-popup-btn-next" id="eikona-popup-btn-next-'  + galleryID + '">></div>' +
                '        <div class="eikona-popup-btn-close" id="eikona-popup-btn-close-'  + galleryID + '">X</div>' +
                '    </div> ' +
                '</div>');

        this.popup.mousemove(() => {
            if (this.currentElem) this.showButtons();
        });

        this.popup.click(() => {
            if (this.currentElem) this.showButtons();
        });

        this.imageHolder = $('#eikona-popup-images-' + galleryID);
        this.btnNext = $('#eikona-popup-btn-next-' + galleryID);
        this.btnPrevious = $('#eikona-popup-btn-previous-' + galleryID);
        this.btnClose = $('#eikona-popup-btn-close-' + galleryID);

        this.btnNext.click(() => {
            this.next();
        });

        this.btnPrevious.click(() => {
            this.previous();
        });

        this.btnClose.click(() => {
            this.close();
        });
    }

    isMouseOver(event, callback, callbackNot=null) {
        for (let elem of this.container.children()) {
            elem = $(elem);
            let p = elem.position();
            if (
                event.pageX > p.left
                && event.pageX < p.left + elem.width()
                && event.pageY > p.top
                && event.pageY < p.top + elem.height()
            ) {
                callback(elem);
            } else if (callbackNot) {
                callbackNot(elem);
            }
        }
        return false;
    }

    open(elem) {
        if (this.popup.is(':hidden')) {
            this.popup.show();
            this.setImg(elem);
            Eikona.openFullScreen();
        }
    }

    setImg(elem) {
        let url = elem.css('background-image').match(/url\(["']?([^"']*)["']?\)/)[1];
        this.imageHolder.fadeOut(200, () => {
            this.currentElem = elem;
            this.imageHolder.css('background-image', 'url(' + url + ')');
            this.imageHolder.fadeIn(200);
            this.showButtons();

        });
    }

    next() {
        if (this.currentElem) {
            let next = this.currentElem.next();
            if (next.length) this.setImg(next);
        }
    }

    previous() {
        if (this.currentElem) {
            let prev = this.currentElem.prev();
            if (prev.length) this.setImg(prev);
        }
    }

    showButtons() {
        (this.currentElem.next().length) ? this.btnNext.fadeIn() : this.btnNext.fadeOut();
        (this.currentElem.prev().length) ? this.btnPrevious.fadeIn() : this.btnPrevious.fadeOut();
        this.btnClose.fadeIn();
        this.startHideBtnsTimer();
    }

    startHideBtnsTimer() {
        if (this.hideBtnsTimer) clearTimeout(this.hideBtnsTimer);
        this.hideBtnsTimer = setTimeout(() => {
            this.hideButtons();
        }, 1700);
    }

    hideButtons() {
        if (
            !(
                this.btnNext.filter(':hover').length
                || this.btnPrevious.filter(':hover').length
                || this.btnClose.filter(':hover').length
            )
            && window.innerWidth > 1024
        ) {
            this.btnNext.fadeOut();
            this.btnPrevious.fadeOut();
            this.btnClose.fadeOut();
        }
    }

    close() {
        if (this.popup.is(':visible')) {
            this.popup.hide();
            this.currentElem = false;
            Eikona.closeFullScreen();
        }
    }

}